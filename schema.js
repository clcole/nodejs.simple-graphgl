const {
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLString,
  GraphQLList,
  GraphQLInt,
  GraphQLNonNull
} = require('graphql');

const authors = [
  {id: 1, name: "Author 1"},
  {id: 2, name: "Author 2"},
  {id: 3, name: "Author 3"},
];

const books = [
  {id: 1, name: "Book 1", authorId: 1},
  {id: 2, name: "Book 2", authorId: 1},
  {id: 3, name: "Book 3", authorId: 2},
  {id: 4, name: "Book 4", authorId: 2},
  {id: 5, name: "Book 5", authorId: 2},
  {id: 6, name: "Book 6", authorId: 3},
];

const AuthorType = new GraphQLObjectType({
  name: "Author",
  description: "An author",
  fields: () => ({
    id: { type: GraphQLNonNull(GraphQLInt) },
    name: { type: GraphQLNonNull(GraphQLString) },
    books: {
      type: new GraphQLList(BookType),
      resolve: (parentAuthor) => {
        return books.filter(book => book.authorId === parentAuthor.id)
      }
    }
  })
});

const BookType = new GraphQLObjectType({
  name: "Book",
  description: "A book",
  fields: () => ({
    id: { type: GraphQLNonNull(GraphQLInt) },
    name: { type: GraphQLNonNull(GraphQLString) },
    authorId: { type: GraphQLNonNull(GraphQLInt) },
    author: {
      type: AuthorType,
      resolve: (parentBook) => {
        return authors.find(author => author.id === parentBook.authorId)
      }
    }
  })
});

const RootQueryType = new GraphQLObjectType({
  name: "Query",
  description: "Root Query",
  fields: () => ({
    book: {
      type: BookType,
      description: "A single book",
      args: {
        id: { type: GraphQLInt }
      },
      resolve: (parent, args) => books.find(book => book.id === args.id)
    },
    books: {
      type: new GraphQLList(BookType),
      description: "List of books",
      resolve: () => books
    },
    author: {
      type: AuthorType,
      description: "A single author",
      args: {
        id: { type: GraphQLInt }
      },
      resolve: (parent, args) => authors.find(author => author.id === args.id)
    },
    authors: {
      type: new GraphQLList(AuthorType),
      description: "List of authors",
      resolve: () => authors
    }
  })
});

const RootMutationType = new GraphQLObjectType({
  name: "Mutation",
  description: "Root Mutation",
  fields: () => ({
    addBook: {
      type: BookType,
      description: "Add a book",
      args: {
        name: { type: GraphQLNonNull(GraphQLString) },
        authorId: { type: GraphQLNonNull(GraphQLInt) }
      },
      resolve: (parent, args) => {
        const book = { id: books.length + 1, name: args.name, authorId: args.authorId };
        books.push(book);
        return book;
      }
    },
    addAuthor: {
      type: AuthorType,
      description: "Add an author",
      args: {
        name: { type: GraphQLNonNull(GraphQLString) }
      },
      resolve: (parent, args) => {
        const author = { id: authors.length + 1, name: args.name };
        authors.push(author);
        return author;
      }
    }
  })
});

module.exports = new GraphQLSchema({
  query: RootQueryType,
  mutation: RootMutationType
});

// module.exports = new GraphQLSchema({
//   query: new GraphQLObjectType({
//     name: "Hello",
//     fields: () => ({
//       message: { 
//         type: GraphQLString,
//         resolve: () => "Hello!"
//       }
//     })
//   })
// });
